import {RESPONSE_FAILURE, RESPONSE_SUCCESS} from '../utils/Const';
import {MatchRepository} from '../repositories/match';
import {RivalRepository} from '../repositories/rival';

export class RoundService {

    static async create(req: any, res: any) {
        const step1: any[] = req.body;

        const categoryId = step1[0].category_id;
        const tournamentId = step1[0].tournament_id;

        const step4 = [ { step: 4, category_id: categoryId, tournament_id: tournamentId, couple: 1 } ];
        const ids4 = await MatchRepository.saveAll(step4);

        const step3 = [
            { step: 3, category_id: categoryId, tournament_id: tournamentId, next_match_id: ids4[0], couple: 1 },
            { step: 3, category_id: categoryId, tournament_id: tournamentId, next_match_id: ids4[0], couple: 2 }
        ];
        const ids3 = await MatchRepository.saveAll(step3);

        const step2 = [
            { step: 2, category_id: categoryId, tournament_id: tournamentId, next_match_id: ids3[0], couple: 1 },
            { step: 2, category_id: categoryId, tournament_id: tournamentId, next_match_id: ids3[0], couple: 2 },
            { step: 2, category_id: categoryId, tournament_id: tournamentId, next_match_id: ids3[1], couple: 3 },
            { step: 2, category_id: categoryId, tournament_id: tournamentId, next_match_id: ids3[1], couple: 4 }
        ];
        const ids2 = await MatchRepository.saveAll(step2);

        step1.forEach(step => {
            step.step = 1;
        });
        step1[0].next_match_id = ids2[0];
        step1[0].couple = 1;
        step1[1].next_match_id = ids2[0];
        step1[1].couple = 2;
        step1[2].next_match_id = ids2[1];
        step1[2].couple = 3;
        step1[3].next_match_id = ids2[1];
        step1[3].couple = 4;
        step1[4].next_match_id = ids2[2];
        step1[4].couple = 5;
        step1[5].next_match_id = ids2[2];
        step1[5].couple = 6;
        step1[6].next_match_id = ids2[3];
        step1[6].couple = 7;
        step1[7].next_match_id = ids2[3];
        step1[7].couple = 8;

        const ids1 = await MatchRepository.saveAll(step1);

        if (ids1 !== undefined && ids1 !== null) {
            res.json(RESPONSE_SUCCESS);
        } else {
            res.status(500).json(RESPONSE_FAILURE);
        }
    }

    static async deleteByCategory(req: any, res: any) {
        const categoryId = req.params.category;
        const length = await MatchRepository.deleteByCategory(categoryId);
        res.json({ delete: length });
    }

    static async deleteByCategoryAndTournament(req: any, res: any) {
        const categoryId = req.params.category;
        const tournamentId = req.params.tournament;
        const length = await MatchRepository.deleteByCategoryAndTournament(categoryId, tournamentId);
        res.json({ delete: length });
    }

    static async getBattlesByCategory(req: any, res: any) {
        const categoryId = req.params.category;
        const matches = await MatchRepository.getByCategoryId(categoryId);
        for (const match of matches) {
            if (match.rival1_id) {
                match.rival1 = await RivalRepository.findById(match.rival1_id);
            }
            if (match.rival2_id) {
                match.rival2 = await RivalRepository.findById(match.rival2_id);
            }
        }
        const step1 = { matches: [], date: new Date() };
        const step2 = { matches: [], date: new Date() };
        const step3 = { matches: [], date: new Date() };
        const step4 = { matches: [], date: new Date() };
        for (const match of matches) {
            if (match.step === 1) {
                // @ts-ignore
                step1.matches.push(match);
                step1.date = match.date;
            } else if (match.step === 2) {
                // @ts-ignore
                step2.matches.push(match);
                step1.date = match.date;
            } else if (match.step === 3) {
                // @ts-ignore
                step3.matches.push(match);
                step1.date = match.date;
            } else if (match.step === 4) {
                // @ts-ignore
                step4.matches.push(match);
                step1.date = match.date;
            }
        }
        const battles: any = {};
        if (step1.matches.length > 0) {
            battles.step1 = step1;
        }
        if (step2.matches.length > 0) {
            battles.step2 = step2;
        }
        if (step3.matches.length > 0) {
            battles.step3 = step3;
        }
        if (step4.matches.length > 0) {
            battles.step4 = step4;
        }
        res.json(battles);
    }

    static async getBattlesByCategoryAndTournament(req: any, res: any) {
        const categoryId = req.params.category;
        const tournamentId = req.params.tournament;
        const matches = await MatchRepository.getByCategoryAndTournament(categoryId, tournamentId);
        for (const match of matches) {
            if (match.rival1_id) {
                match.rival1 = await RivalRepository.findById(match.rival1_id);
            }
            if (match.rival2_id) {
                match.rival2 = await RivalRepository.findById(match.rival2_id);
            }
        }
        const step1 = { matches: [], date: new Date() };
        const step2 = { matches: [], date: new Date() };
        const step3 = { matches: [], date: new Date() };
        const step4 = { matches: [], date: new Date() };
        for (const match of matches) {
            if (match.step === 1) {
                // @ts-ignore
                step1.matches.push(match);
                step1.date = match.date;
            } else if (match.step === 2) {
                // @ts-ignore
                step2.matches.push(match);
                step1.date = match.date;
            } else if (match.step === 3) {
                // @ts-ignore
                step3.matches.push(match);
                step1.date = match.date;
            } else if (match.step === 4) {
                // @ts-ignore
                step4.matches.push(match);
                step1.date = match.date;
            }
        }
        const battles: any = {};
        if (step1.matches.length > 0) {
            battles.step1 = step1;
        }
        if (step2.matches.length > 0) {
            battles.step2 = step2;
        }
        if (step3.matches.length > 0) {
            battles.step3 = step3;
        }
        if (step4.matches.length > 0) {
            battles.step4 = step4;
        }
        res.json(battles);
    }
}
