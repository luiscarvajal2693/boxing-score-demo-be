import {RESPONSE_FAILURE, RESPONSE_SUCCESS} from '../utils/Const';
import {MatchRepository} from '../repositories/match';
import {RivalRepository} from '../repositories/rival';

export class MatchService {

    static async getAll(req: any, res: any) {
        const matches = await MatchRepository.getAll();
        res.json(matches);
    }

    static async getByCategory(req: any, res: any) {
        const categoryId = req.params.category;
        const matches = await MatchRepository.getByCategoryId(categoryId);
        for (const match of matches) {
            if (match.rival1_id) {
                match.rival1 = await RivalRepository.findById(match.rival1_id);
            }
            if (match.rival2_id) {
                match.rival2 = await RivalRepository.findById(match.rival2_id);
            }
        }
        res.status(200).json(matches);
    }

    static async getByCategoryAndTournament(req: any, res: any) {
        const categoryId = req.params.category;
        const tournamentId = req.params.tournament;
        const matches = await MatchRepository.getByCategoryAndTournament(categoryId, tournamentId);
        for (const match of matches) {
            if (match.rival1_id) {
                match.rival1 = await RivalRepository.findById(match.rival1_id);
            }
            if (match.rival2_id) {
                match.rival2 = await RivalRepository.findById(match.rival2_id);
            }
        }
        res.status(200).json(matches);
    }

    static async findById(req: any, res: any) {
        const id = req.params.id;
        const match = await MatchRepository.findById(id);
        if (match !== undefined && match !== null) {
            res.json(match);
        } else {
            res.status(404).json(RESPONSE_FAILURE);
        }
    }

    static async save(req: any, res: any) {
        let match = req.body;
        match = await MatchRepository.save(match);
        if (match !== undefined && match !== null) {
            res.json(match);
        } else {
            res.status(500).json(RESPONSE_FAILURE);
        }
    }

    static async update(req: any, res: any) {
        try {
            let match = req.body;
            const id = match.id;
            await MatchRepository.update(id, match);
            match = await MatchRepository.findById(id);
            if (match.winner == 1 || match.winner == 2) {
                const nextMatch = await MatchRepository.findById(match.next_match_id);
                if (!!(Number(match.couple) % 2)) {
                    nextMatch.rival1_id = match.winner == 1 ? match.rival1_id : match.rival2_id;
                } else if (!(Number(match.couple) % 2)) {
                    nextMatch.rival2_id = match.winner == 1 ? match.rival1_id : match.rival2_id;
                }
                await MatchRepository.update(nextMatch.id, nextMatch);
            }
            res.json(match);
        } catch (e) {
            res.status(404).json(RESPONSE_FAILURE);
        }
    }

    static async delete(req: any, res: any) {
        const id = req.params.id;
        if (await MatchRepository.delete(id)) {
            res.status(200).json(RESPONSE_SUCCESS);
        } else {
            res.status(404).json(RESPONSE_FAILURE);
        }
    }
}
