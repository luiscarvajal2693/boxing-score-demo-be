import {RESPONSE_FAILURE, RESPONSE_SUCCESS} from '../utils/Const';
import {RivalRepository} from '../repositories/rival';
import {CategoryRepository} from '../repositories/category';
import {RivalValidation} from '../utils/validation';

export class RivalService {

    static async getAll(req: any, res: any) {
        const rivals = await RivalRepository.getAll();
        res.json(rivals);
    }

    static async findById(req: any, res: any) {
        const id = req.params.id;
        const rival = await RivalRepository.findById(id);
        if (rival !== undefined && rival !== null) {
            res.json(rival);
        } else {
            res.status(404).json(RESPONSE_FAILURE);
        }
    }

    static async getByCategory(req: any, res: any) {
        const categoryId = req.params.category;
        const category: any = await CategoryRepository.findById(categoryId);
        if (category !== null) {
            const weight = category.weight;
            const sex = category.sex;
            const heavier = category.heavier;
            let rivals: any;
            if (heavier) {
                rivals = await RivalRepository.getBySexAndWeightHeavier(sex, weight);
            } else {
                rivals = await RivalRepository.getBySexAndWeight(sex, weight);
            }
            res.json(rivals);
        } else {
            res.status(404).json(RESPONSE_FAILURE);
        }
    }

    static async search(req: any, res: any) {
        const weightMin = Number(req.query.weightMin);
        const weightMax = Number(req.query.weightMax);
        const sex = req.query.sex;
        const rivals = await RivalRepository.getByCriteria(weightMin, weightMax, sex);
        res.json(rivals);
    }

    static async save(req: any, res: any) {
        let rival = req.body;
        if (RivalValidation.isValid(rival, res)) {
            rival = await RivalRepository.save(rival);
            if (rival !== undefined && rival !== null) {
                res.json(rival);
            } else {
                res.status(500).json(RESPONSE_FAILURE);
            }
        }
    }

    static async saveAll(req: any, res: any) {
        const rivals: any[] = req.body;
        const ids = await RivalRepository.saveAll(rivals);
        if (ids !== undefined && ids !== null) {
            res.json(ids);
        } else {
            res.status(500).json(RESPONSE_FAILURE);
        }
    }

    static async update(req: any, res: any) {
        let rival = req.body;
        const id = rival.id;
        await RivalRepository.update(id, rival);
        rival = await RivalRepository.findById(id);
        if (rival !== undefined && rival !== null) {
            res.json(rival);
        } else {
            res.status(404).json(RESPONSE_FAILURE);
        }
    }

    static async delete(req: any, res: any) {
        const id = req.params.id;
        if (await RivalRepository.delete(id)) {
            res.status(200).json(RESPONSE_SUCCESS);
            } else {
            res.status(404).json(RESPONSE_FAILURE);
        }
    }
}
