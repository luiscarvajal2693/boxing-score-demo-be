import {RESPONSE_FAILURE} from '../utils/Const';
import {AuthenticateRepository} from "../repositories/authenticate";

export class AuthenticateService {

    static signIn(req: any, res: any) {
        const username = req.body.username;
        const password = req.body.password;
        AuthenticateRepository.getByUsername(username)
            .then(value => {
                if (value) {
                    if (value.password === password) {
                        res.json(value);
                    } else {
                        const response: any = RESPONSE_FAILURE;
                        response.reason = 'invalid password';
                        res.status(401).json(response);
                    }
                } else {
                    const response: any = RESPONSE_FAILURE;
                    response.reason = 'invalid username';
                    res.status(401).json(response);
                }
            }).catch(reason => {
                res.status(500).json(RESPONSE_FAILURE);
            });

    }
}
