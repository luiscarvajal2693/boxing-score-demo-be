import {RESPONSE_FAILURE, RESPONSE_SUCCESS} from '../utils/Const';
import {CategoryRepository} from '../repositories/category';
import {MatchRepository} from "../repositories/match";

export class CategoryService {

    static async getAll(req: any, res: any) {
        const categories = await CategoryRepository.getAll();
        res.json(categories);
    }

    static async getBySex(req: any, res: any) {
        const sex = req.params.sex;
        const categories = await CategoryRepository.getBySex(sex);
        res.json(categories);
    }

    static async findById(req: any, res: any) {
        const id = req.params.id;
        const rival = await CategoryRepository.findById(id);
        if (rival !== undefined && rival !== null) {
            res.json(rival);
        } else {
            res.status(404).json(RESPONSE_FAILURE);
        }
    }

    static async save(req: any, res: any) {
        let category = req.body;
        category = await MatchRepository.save(category);
        if (category !== undefined && category !== null) {
            res.json(category);
        } else {
            res.status(500).json(RESPONSE_FAILURE);
        }
    }

    static async saveAll(req: any, res: any) {
        const categories: any[] = req.body;
        const ids = await CategoryRepository.saveAll(categories);
        if (ids !== undefined && ids !== null) {
            res.json(ids);
        } else {
            res.status(500).json(RESPONSE_FAILURE);
        }
    }

    static async update(req: any, res: any) {
        let rival = req.body;
        const id = rival.id;
        await CategoryRepository.update(id, rival);
        rival = await CategoryRepository.findById(id);
        if (rival !== undefined && rival !== null) {
            res.json(rival);
        } else {
            res.status(404).json(RESPONSE_FAILURE);
        }
    }

    static async delete(req: any, res: any) {
        const id = req.params.id;
        if (await CategoryRepository.delete(id)) {
            res.status(200).json(RESPONSE_SUCCESS);
            } else {
            res.status(404).json(RESPONSE_FAILURE);
        }
    }
}
