import {RESPONSE_FAILURE, RESPONSE_SUCCESS} from '../utils/Const';
import {TournamentRepository} from '../repositories/tournament';

export class TournamentService {

    static async getAll(req: any, res: any) {
        const tournaments = await TournamentRepository.getAll();
        res.json(tournaments);
    }

    static async findById(req: any, res: any) {
        const id = req.params.id;
        const tournament = await TournamentRepository.findById(id);
        if (tournament !== undefined && tournament !== null) {
            res.json(tournament);
        } else {
            res.status(404).json(RESPONSE_FAILURE);
        }
    }

    static async save(req: any, res: any) {
        let tournament = req.body;
        tournament = await TournamentRepository.save(tournament);
        if (tournament !== undefined && tournament !== null) {
            res.json(tournament);
        } else {
            res.status(500).json(RESPONSE_FAILURE);
        }
    }

    static async saveAll(req: any, res: any) {
        const tournaments: any[] = req.body;
        const ids = await TournamentRepository.saveAll(tournaments);
        if (ids !== undefined && ids !== null) {
            res.json(ids);
        } else {
            res.status(500).json(RESPONSE_FAILURE);
        }
    }

    static async update(req: any, res: any) {
        let tournament = req.body;
        const id = tournament.id;
        await TournamentRepository.update(id, tournament);
        tournament = await TournamentRepository.findById(id);
        if (tournament !== undefined && tournament !== null) {
            res.json(tournament);
        } else {
            res.status(404).json(RESPONSE_FAILURE);
        }
    }

    static async delete(req: any, res: any) {
        const id = req.params.id;
        if (await TournamentRepository.delete(id)) {
            res.status(200).json(RESPONSE_SUCCESS);
            } else {
            res.status(404).json(RESPONSE_FAILURE);
        }
    }
}
