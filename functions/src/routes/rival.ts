import * as express from 'express';
import * as asyncHandler from 'express-async-handler'
import {RivalService} from '../services/rival';
import {availableCors} from "../utils/Const";

const basePath = '/api';
const app = express();

app.get(`${basePath}/get-all`, asyncHandler(async (req, res) => {
    availableCors(res);
    await RivalService.getAll(req, res);
}));

app.get(`${basePath}/get-by-id/:id`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await RivalService.findById(req, res);
}));

app.get(`${basePath}/get-by-category/:category`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await RivalService.getByCategory(req, res);
}));

app.get(`${basePath}/search`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await RivalService.search(req, res);
}));

app.post(`${basePath}/save`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await RivalService.save(req, res);
}));

app.post(`${basePath}/save-all`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await RivalService.saveAll(req, res);
}));

app.put(`${basePath}/update`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await RivalService.update(req, res);
}));

app.delete(`${basePath}/delete/:id`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await RivalService.delete(req, res);
}));

export const rivalRoute = app;
