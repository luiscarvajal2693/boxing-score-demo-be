import * as express from 'express';
import {RoundService} from '../services/round';
import {availableCors} from "../utils/Const";

const basePath = '/api';
const app = express();

app.post(`${basePath}/create`, async (req, res)  => {
    availableCors(res);
    await RoundService.create(req, res);
});

app.delete(`${basePath}/delete-by-category/:category`, async (req, res)  => {
    availableCors(res);
    await RoundService.deleteByCategory(req, res);
});

app.delete(`${basePath}/delete-by-category/:category/tournament/:tournament`, async (req, res)  => {
    availableCors(res);
    await RoundService.deleteByCategoryAndTournament(req, res);
});

app.get(`${basePath}/battles-by-category/:category`, async (req, res)  => {
    availableCors(res);
    await RoundService.getBattlesByCategory(req, res);
});

app.get(`${basePath}/battles-by-category/:category/tournament/:tournament`, async (req, res)  => {
    availableCors(res);
    await RoundService.getBattlesByCategoryAndTournament(req, res);
});

export const roundRoute = app;
