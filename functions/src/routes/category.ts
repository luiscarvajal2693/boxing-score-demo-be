import * as express from 'express';
import * as asyncHandler from 'express-async-handler'
import {CategoryService} from '../services/category';
import {availableCors} from "../utils/Const";

const basePath = '/api';
const app = express();

app.get(`${basePath}/get-all`, asyncHandler(async (req, res) => {
    availableCors(res);
    await CategoryService.getAll(req, res);
}));

app.get(`${basePath}/get-by-sex/:sex`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await CategoryService.getBySex(req, res);
}));

app.get(`${basePath}/get-by-id/:id`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await CategoryService.findById(req, res);
}));

app.post(`${basePath}/save`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await CategoryService.save(req, res);
}));

app.post(`${basePath}/save-all`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await CategoryService.saveAll(req, res);
}));

app.put(`${basePath}/update`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await CategoryService.update(req, res);
}));

app.delete(`${basePath}/delete/:id`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await CategoryService.delete(req, res);
}));

export const categoryRoute = app;
