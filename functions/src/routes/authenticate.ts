import * as express from 'express';
import {AuthenticateService} from "../services/authenticate";
import {availableCors} from "../utils/Const";

const basePath = '/api';
const app = express();

app.post(`${basePath}/sign-in`, async (req, res)  => {
    availableCors(res);
    AuthenticateService.signIn(req, res);
});

export const authRoute = app;
