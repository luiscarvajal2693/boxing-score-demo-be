import * as express from 'express';
import * as asyncHandler from 'express-async-handler'
import {MatchService} from '../services/match';
import {availableCors} from "../utils/Const";

const basePath = '/api';
const app = express();

app.get(`${basePath}/get-all`, asyncHandler(async (req, res) => {
    availableCors(res);
    await MatchService.getAll(req, res);
}));

app.get(`${basePath}/get-by-id/:id`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await MatchService.findById(req, res);
}));

app.get(`${basePath}/get-by-category/:category`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await MatchService.getByCategory(req, res);
}));

app.get(`${basePath}/get-by-category/:category/tournament/:tournament`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await MatchService.getByCategoryAndTournament(req, res);
}));

app.post(`${basePath}/save`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await MatchService.save(req, res);
}));

app.put(`${basePath}/update`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await MatchService.update(req, res);
}));

app.delete(`${basePath}/delete/:id`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await MatchService.delete(req, res);
}));

export const matchRoute = app;
