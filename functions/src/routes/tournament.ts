import * as express from 'express';
import * as asyncHandler from 'express-async-handler'
import {TournamentService} from '../services/tournament';
import {availableCors} from "../utils/Const";

const basePath = '/api';
const app = express();

app.get(`${basePath}/get-all`, asyncHandler(async (req, res) => {
    availableCors(res);
    await TournamentService.getAll(req, res);
}));

app.get(`${basePath}/get-by-id/:id`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await TournamentService.findById(req, res);
}));

app.post(`${basePath}/save`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await TournamentService.save(req, res);
}));

app.post(`${basePath}/save-all`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await TournamentService.saveAll(req, res);
}));

app.put(`${basePath}/update`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await TournamentService.update(req, res);
}));

app.delete(`${basePath}/delete/:id`, asyncHandler(async (req, res)  => {
    availableCors(res);
    await TournamentService.delete(req, res);
}));

export const tournamentRoute = app;
