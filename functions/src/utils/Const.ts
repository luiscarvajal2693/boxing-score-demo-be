

export const RESPONSE_SUCCESS = { response: 'success' };
export const RESPONSE_FAILURE = { response: 'failure' };

export function availableCors(res: any) {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.set('Access-Control-Allow-Headers', '*');
}
