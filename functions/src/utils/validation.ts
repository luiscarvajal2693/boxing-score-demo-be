import {RESPONSE_FAILURE} from './Const';

export class RivalValidation {

    static isValid(rival: any, res: any) {
        const response: any = RESPONSE_FAILURE;
        if (rival.weight === undefined
            || rival.weight === null
            || typeof rival.weight !== 'number') {
            response.error = 'weight should be numeric and cannot be null';
            res.status(422).json(response);
            return false;
        } else if (rival.sex === undefined
            || rival.sex === null
            || (rival.sex !== 'M' && rival.sex !== 'F')) {
            response.error = 'sex should be M or F and cannot be null';
            res.status(422).json(response);
            return false;
        }
        return true;
    }

}
