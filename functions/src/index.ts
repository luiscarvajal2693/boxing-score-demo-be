import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as cors from 'cors';
import {rivalRoute} from './routes/rival';
import {matchRoute} from './routes/match';
import {roundRoute} from './routes/round';
import {categoryRoute} from './routes/category';
import {authRoute} from "./routes/authenticate";
import {tournamentRoute} from "./routes/tournament";
import * as express from 'express';

// Initializes Cloud Functions.
admin.initializeApp(functions.config().firebase);

// Firestore settings.
export const db = admin.firestore();
db.settings({ timestampsInSnapshots: true });

// Express settings.
const app = express();
const corsHandler = cors({origin: true});

// auth route
authRoute.use(corsHandler);
authRoute.use('', app);

// rival route
rivalRoute.use(corsHandler);
rivalRoute.use('', app);

// match route
matchRoute.use(corsHandler);
matchRoute.use('', app);

// round route
roundRoute.use(corsHandler);
roundRoute.use('', app);

// category route
categoryRoute.use(corsHandler);
categoryRoute.use('', app);

// tournament route
tournamentRoute.use(corsHandler);
tournamentRoute.use('', app);

export const auth = functions.https.onRequest(authRoute);

export const rival = functions.https.onRequest(rivalRoute);

export const match = functions.https.onRequest(matchRoute);

export const round = functions.https.onRequest(roundRoute);

export const category = functions.https.onRequest(categoryRoute);

export const tournament = functions.https.onRequest(tournamentRoute);

