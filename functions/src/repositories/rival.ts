import {BaseRepository} from './base';
import {db} from '../index';

export class RivalRepository extends BaseRepository {

    static entity = 'rivals';

    static async getByCriteria(weightMin: number, weightMax: number, sex: string) {
        const snapshot = await db.collection(this.entity)
            .where('weight', '>=', weightMin)
            .where('weight', '<=', weightMax)
            .where('sex', '==', sex).get();
        const rivals: any[] = [];
        snapshot.forEach(doc => {
            rivals.push(this.mapDocToObject(doc));
        });
        return rivals;
    }

    static async getBySexAndWeight(sex: string, weight: number) {
        const snapshot = await db.collection(this.entity)
            .where('weight', '==', weight)
            .where('sex', '==', sex).get();
        const rivals: any[] = [];
        snapshot.forEach(doc => {
            rivals.push(this.mapDocToObject(doc));
        });
        return rivals;
    }

    static async getBySexAndWeightHeavier(sex: string, weight: number) {
        const snapshot = await db.collection(this.entity)
            .where('weight', '>=', weight)
            .where('sex', '==', sex).get();
        const rivals: any[] = [];
        snapshot.forEach(doc => {
            rivals.push(this.mapDocToObject(doc));
        });
        return rivals;
    }

}
