import {BaseRepository} from './base';
import {db} from "../index";

export class AuthenticateRepository extends BaseRepository {

    static entity = 'users';

    static async getByUsername(username: string) {
        const snapshot = await db.collection(this.entity)
            .where('username', '==', username).get();
        let user: any = null;
        snapshot.forEach(doc => {
            user = this.mapDocToObject(doc);
        });
        return user;
    }

}
