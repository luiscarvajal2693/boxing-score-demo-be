import {db} from '../index';

export abstract class BaseRepository {

    static entity = '';

    static mapDocToObject(doc: any) {
        const object = doc.data();
        object.id = doc.id;
        return object;
    }

    static async getAll() {
        const snapshot = await db.collection(this.entity).get();
        const list: any[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async findById(id: string) {
        const doc = await db.collection(this.entity).doc(id).get();
        if (doc.exists) {
            return this.mapDocToObject(doc);
        } else {
            return null;
        }
    }

    static async save(object: any) {
        const objectRef = await db.collection(this.entity).add(object);
        object.id = objectRef.id;
        return object;
    }

    static async saveAll(list: any[]) {
        const batch = db.batch();
        const ids: any[] = [];
        list.forEach(object => {
            const objectRef = db.collection(this.entity).doc();
            batch.set(objectRef, object);
            ids.push(objectRef.id);
        });

        await batch.commit();
        return ids;
    }

    static async update(id: string, object: any) {
        delete object.id;
        await db.collection(this.entity).doc(id).update(object);
        object.id = id;
        return object;
    }

    static async delete(id: string) {
        await db.collection(this.entity).doc(id).delete();
        return true;
    }
}
