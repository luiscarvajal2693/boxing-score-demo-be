import {BaseRepository} from './base';
import {db} from '../index';

export class MatchRepository extends BaseRepository {

    static entity = 'matches';

    static mapDocToObject(doc: any) {
        const object = doc.data();
        object.id = doc.id;
        return object;
    }

    static async save(match: any) {
        const matchRef = await db.collection(this.entity).add(match);
        match.id = matchRef.id;
        return match;
    }

    static async getByCategoryId(categoryId: string) {
        const snapshot = await db.collection(this.entity)
            .where('category_id', '==', categoryId)
            .orderBy('couple', 'asc')
            .get();
        const matches: any[] = [];
        snapshot.forEach((doc: any) => {
            matches.push(this.mapDocToObject(doc));
        });
        return matches;
    }

    static async getByCategoryAndTournament(categoryId: string, tournamentId: string) {
        const snapshot = await db.collection(this.entity)
            .where('category_id', '==', categoryId)
            .where('tournament_id', '==', tournamentId)
            .orderBy('couple', 'asc')
            .get();
        const matches: any[] = [];
        snapshot.forEach((doc: any) => {
            matches.push(this.mapDocToObject(doc));
        });
        return matches;
    }

    static async deleteByCategory(categoryId: string) {
        const batch = db.batch();
        const snapshot = await db.collection(this.entity)
            .where('category_id', '==', categoryId).get();
        snapshot.docs.forEach(doc => {
            batch.delete(doc.ref);
        });
        await batch.commit();
        return snapshot.size;
    }

    static async deleteByCategoryAndTournament(categoryId: string, tournamentId: string) {
        const batch = db.batch();
        const snapshot = await db.collection(this.entity)
            .where('category_id', '==', categoryId)
            .where('tournament_id', '==', tournamentId).get();
        snapshot.docs.forEach(doc => {
            batch.delete(doc.ref);
        });
        await batch.commit();
        return snapshot.size;
    }
}
