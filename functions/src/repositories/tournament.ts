import {BaseRepository} from './base';

export class TournamentRepository extends BaseRepository {

    static entity = 'tournaments';

}
