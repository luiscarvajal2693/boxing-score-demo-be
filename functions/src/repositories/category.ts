import {BaseRepository} from './base';
import {db} from "../index";

export class CategoryRepository extends BaseRepository {

    static entity = 'categories';

    static async getAll() {
        const snapshot = await db.collection(this.entity).orderBy('weight').get();
        const list: any[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async getBySex(sex: string) {
        const snapshot = await db.collection(this.entity).where('sex', '==', sex).orderBy('weight').get();
        const list: any[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }
}
